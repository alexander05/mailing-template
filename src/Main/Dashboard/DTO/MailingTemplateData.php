<?php
namespace Main\Dashboard\DTO;

class MailingTemplateData
{
    /** @var int */
    public $id;

    /** @var int */
    public $panelId;

    /** @var int */
    public $senderId;

    /** @var string */
    public $title;

    /** @var string */
    public $subject;

    /** @var string */
    public $replyTo;
}
