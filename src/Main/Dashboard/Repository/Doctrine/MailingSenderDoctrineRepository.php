<?php
namespace Main\Dashboard\Repository\Doctrine;

use Main\Dashboard\DTO\MailingSenderData;
use Main\Dashboard\Repository\MailingSenderRepository;
use PDO;

class MailingSenderDoctrineRepository extends BaseDoctrineRepository implements MailingSenderRepository
{
    const TABLE = '"mailing_sender"';

    const FIELDS = [
        'sender_id',
        'email',
        'name',
    ];

    /**
     * {@inheritdoc}
     */
    public function findById(int $id): ?MailingSenderData
    {
        return $this->findBy('sender_id', $id);
    }

    /**
     * {@inheritdoc}
     */
    public function all(): array
    {
        $stmt = $this->getQueryBuilder()
            ->select(self::FIELDS)
            ->from(self::TABLE)
            ->execute();

        $list = [];
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $data = $this->buildData($row);
            $list[] = $data;
        }

        return $list;
    }

    /**
     * {@inheritdoc}
     */
    public function create(MailingSenderData $data): MailingSenderData
    {
        $this->getConnection()->insert(
            self::TABLE,
            [
                'email' => $data->email,
                'name' =>  $data->name,
            ]
        );

        $mailingSenderData = clone $data;
        $mailingSenderData->id = $this->getConnection()->lastInsertId();

        return $mailingSenderData;
    }

    /**
     * {@inheritdoc}
     */
    public function update(MailingSenderData $data): bool
    {
        return (bool) $this->getConnection()->update(
            self::TABLE,
            [
                'email' => $data->email,
                'name' => $data->name,
            ],
            [
                'sender_id' => $data->id,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function remove(int $id)
    {
        $this->getConnection()->delete(self::TABLE, ['sender_id' => $id]);
    }

    /**
     * @param string $key
     * @param mixed $value
     *
     * @return MailingSenderData|null
     */
    private function findBy(string $key, $value): ?MailingSenderData
    {
        $row = $this->getQueryBuilder()
            ->select(self::FIELDS)
            ->from(self::TABLE)
            ->where(sprintf('%s = :value', $key))
            ->setParameter('value', $value)
            ->execute()
            ->fetch(PDO::FETCH_ASSOC);

        return $row ? $this->buildData($row) : null;
    }

    /**
     * @param array $row
     *
     * @return MailingSenderData
     */
    private function buildData(array $row): MailingSenderData
    {
        $data = new MailingSenderData();
        $data->id = (int) $row['sender_id'];
        $data->email = $row['email'];
        $data->name = $row['name'];

        return $data;
    }
}
