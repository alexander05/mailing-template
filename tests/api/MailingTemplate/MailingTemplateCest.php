<?php

class MailingTemplateCest
{
    public function _before(ApiTester $i)
    {
        $i->loadFixtures('MailingTemplate');
    }

    /**
     * @param ApiTester $i
     *
     */
    public function tryToListMailingTemplate(ApiTester $i)
    {
        $i->amAuthenticatedByApiKey('has-role-manage-mailing-template@mail.ru', '12345');
        $i->wantTo('get mailing template list via API from /mailing/template/list');

        $i->testApiRequest(
            '/mailing/template/list',
            [],
            ['result' => 'success']
        );

        $data = $i->grabDataFromResponseByJsonPath('$.data[*]');
        $i->assertEquals([
            [
                'id' => 1,
                'panelId'=> 1,
                'senderId'=> 1,
                'title' => 'title1',
                'subject'=> 'subject1',
                'replyTo'=> 'reply1',
                'bodyText' => 'bodytext1',
                'bodyHtml' => 'bodyhtml1'
            ],
            [
                'id' => 2,
                'panelId'=> 1,
                'senderId'=> 1,
                'title' => 'title2',
                'subject'=> 'subject2',
                'replyTo'=> 'reply2',
                'bodyText' => 'bodytext2',
                'bodyHtml' => 'bodyhtml2'
            ],
            [
                'id' => 3,
                'panelId'=> 1,
                'senderId'=> 1,
                'title' => 'title3',
                'subject'=> 'subject3',
                'replyTo'=> 'reply3',
                'bodyText' => 'bodytext3',
                'bodyHtml' => 'bodyhtml3'
            ],
            [
                'id' => 4,
                'panelId'=> 1,
                'senderId'=> 1,
                'title' => 'title1',
                'subject'=> 'subject4',
                'replyTo'=> 'reply4',
                'bodyText' => 'bodytext4',
                'bodyHtml' => 'bodyhtml4'
            ],
        ], $data);

    }

    /**
     * @param ApiTester $i
     */
    public function tryToGetMailingTemplate(ApiTester $i)
    {
        $i->amAuthenticatedByApiKey('has-role-manage-mailing-template@mail.ru', '12345');
        $i->wantTo('get mailing template via API from /mailing/template/{id}');
        $i->testApiRequest(
            '/mailing/template/2',
            [],
            [
                'result' => 'success',
                'data' => [
                    'id' => 2,
                    'panelId'=> 1,
                    'senderId'=> 1,
                    'title' => 'title2',
                    'subject'=> 'subject2',
                    'replyTo'=> 'reply2',
                    'bodyText' => 'bodytext2',
                    'bodyHtml' => 'bodyhtml2'
                ],
            ]
        );
    }

}