<?php

class FilterMailingTemplateCest
{
    public function _before(ApiTester $i)
    {
        $i->loadFixtures('MailingTemplate');
    }

    /**
     * @param ApiTester $i
     *
     */
    public function tryToFilter(ApiTester $i)
    {
        $i->amAuthenticatedByApiKey('list-all-mailing-template@mail.ru', '12345');
        $i->wantTo('Filter mailing template via API from /mailing/template/filter');
        $i->testApiRequest(
            '/mailing/template/filter',
            [
                'payload' => [
                    'filter' => [
                        'title' => 'title1',
                        'panelId' => 1,
                        'senderId' => 1,
                    ],
                    'orderBy' => [
                        'field'=> 'subject',
                        'order' => 'DESC',
                    ],
                ],
            ],
            [
                'result' => 'success',
                'data' =>  [4,1]
            ]
        );
    }
}