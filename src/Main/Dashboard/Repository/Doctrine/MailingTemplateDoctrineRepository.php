<?php
namespace Main\Dashboard\Repository\Doctrine;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Main\Dashboard\DTO\MailingFilterData;
use Main\Dashboard\DTO\MailingSenderData;
use Main\Dashboard\DTO\MailingTemplateFullData;
use Main\Dashboard\DTO\MailingTemplateWithBodyData;
use Main\Dashboard\DTO\SortOrderData;
use Main\Dashboard\Repository\MailingTemplateRepository;
use PDO;

class MailingTemplateDoctrineRepository extends BaseDoctrineRepository implements MailingTemplateRepository
{
    const TABLE = '"mailing_template"';
    const SENDER_TABLE = '"mailing_sender"';

    const FIELDS = [
        'panel_id',
        'sender_id',
        'title',
        'subject',
        'body_text',
        'body_html',
        'reply_to',
    ];

    private const SORT_FIELD_MAP = [
        'id' => 'mt.mailing_template_id',
        'panelId' => 'mt.panel_id',
        'senderId' => 'mt.sender_id',
        'title' => 'mt.title',
        'subject' => 'mt.subject',
        'replyTo' => 'mt.reply_to',
    ];

    /**
     * {@inheritdoc}
     */
    public function findById(int $id): ?MailingTemplateWithBodyData
    {
        return $this->findBy('mailing_template_id', $id);
    }

    /**
     * {@inheritdoc}
     */
    public function all(): array
    {
        $stmt = $this->getQueryBuilder()
            ->select(
                array_merge(
                    ['mailing_template_id'],
                    self::FIELDS
                )
            )
            ->from(self::TABLE)
            ->execute();

        $list = [];
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $data = $this->buildData($row);
            $list[] = $data;
        }

        return $list;
    }

    /**
     * {@inheritdoc}
     */
    public function listFullDataByIds(array $ids): array
    {
        $stmt = $this->getQueryBuilder()
            ->select([
                't.mailing_template_id',
                't.panel_id',
                't.sender_id',
                't.title',
                't.subject',
                't.body_text',
                't.body_html',
                't.reply_to',
                's.name',
                's.email',
            ])
            ->from(self::TABLE, 't')
            ->innerJoin('t', MailingSenderDoctrineRepository::TABLE, 's', 't.sender_id = s.sender_id')
            ->where('t.mailing_template_id IN (:ids)')
            ->setParameter('ids', $ids, Connection::PARAM_INT_ARRAY)
            ->execute();

        $list = [];
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $list[$row['mailing_template_id']] = $this->buildFullData($row);
        }

        return $list;
    }

    /**
     * {@inheritdoc}
     */
    public function create(MailingTemplateWithBodyData $data): MailingTemplateWithBodyData
    {
        $this->getConnection()->insert(
            self::TABLE,
            [
                'panel_id' => (int) $data->panelId,
                'sender_id' => (int) $data->senderId,
                'title' => $data->title,
                'subject' => $data->subject,
                'body_text' => $data->bodyText,
                'body_html' => $data->bodyHtml,
                'reply_to' => $data->replyTo,
            ]
        );

        $mailingTemplateWithBodyData = clone $data;
        $mailingTemplateWithBodyData->id = $this->getConnection()->lastInsertId();

        return $mailingTemplateWithBodyData;
    }

    /**
     * {@inheritdoc}
     */
    public function update(MailingTemplateWithBodyData $data): bool
    {
        return (bool) $this->getConnection()->update(
            self::TABLE,
            [
                'panel_id' => (int) $data->panelId,
                'sender_id' => (int) $data->senderId,
                'title' => $data->title,
                'subject' => $data->subject,
                'body_text' => $data->bodyText,
                'body_html' => $data->bodyHtml,
                'reply_to' => $data->replyTo,
            ],
            [
                'mailing_template_id' => $data->id,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function remove(int $id)
    {
        $this->getConnection()->delete(self::TABLE, ['mailing_template_id' => $id]);
    }

    /**
     * {@inheritdoc}
     */
    public function filter(MailingFilterData $mailingFilterData, SortOrderData $sortOrderData): array
    {
        $query = $this->getQueryBuilder()
            ->select('mt.mailing_template_id')
            ->from(self::TABLE, 'mt')
            ->leftJoin('mt', self::SENDER_TABLE, 'ms', 'mt.sender_id = ms.sender_id');

        if (array_key_exists($sortOrderData->field, self::SORT_FIELD_MAP)) {
            $query = $this->buildFilterOrderBy($sortOrderData, $query);
        }

        $query = $this->buildFilterWhere($mailingFilterData, $query);

        $stmt = $query->execute();

        $list = [];
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $list[] = $row['mailing_template_id'];
        }

        return $list;
    }

    /**
     * @param SortOrderData $sortOrderData
     * @param QueryBuilder  $query
     *
     * @return QueryBuilder $query
     */
    private function buildFilterOrderBy($sortOrderData, $query): QueryBuilder
    {
        $sortField = self::SORT_FIELD_MAP[$sortOrderData->field];
        $sortOrder = strtolower($sortOrderData->order) === strtolower(SortOrderData::ORDER_DESC)
            ? SortOrderData::ORDER_DESC
            : SortOrderData::ORDER_ASC;

        $query->orderBy($sortField, $sortOrder);

        return $query;
    }

    /**
     * @param MailingFilterData $mailingFilterData
     * @param QueryBuilder      $query
     *
     * @return QueryBuilder $query
     */
    private function buildFilterWhere(MailingFilterData $mailingFilterData, QueryBuilder $query): QueryBuilder
    {
        if ($mailingFilterData->senderId) {
            $query->andWhere('mt.sender_id = :sender_id')
                ->setParameter('sender_id', $mailingFilterData->senderId);
        }

        if ($mailingFilterData->panelId) {
            $query->andWhere('mt.panel_id = :panel_id')
                ->setParameter('panel_id', $mailingFilterData->panelId);
        }

        if ($mailingFilterData->title) {
            $query->andWhere('mt.title ILIKE :title')
                ->setParameter('title', sprintf('%%%s%%', $mailingFilterData->title));
        }

        if ($mailingFilterData->senderEmail) {
            $query->andWhere('ms.email ILIKE :email')
                ->setParameter('email', sprintf('%%%s%%', $mailingFilterData->senderEmail));
        }

        return $query;
    }

    /**
     * @param string $key
     * @param mixed $value
     *
     * @return MailingTemplateWithBodyData|null
     */
    private function findBy(string $key, $value): ?MailingTemplateWithBodyData
    {
        $row = $this->getQueryBuilder()
            ->select(
                [
                    'mailing_template_id',
                    'panel_id',
                    'sender_id',
                    'title',
                    'subject',
                    'body_text',
                    'body_html',
                    'reply_to',
                ]
            )
            ->from(self::TABLE)
            ->where(sprintf('%s = :value', $key))
            ->setParameter('value', $value)
            ->execute()
            ->fetch(PDO::FETCH_ASSOC);

        return $row ? $this->buildData($row) : null;
    }

    /**
     * @param array $row
     *
     * @return MailingTemplateWithBodyData
     */
    private function buildData(array $row): MailingTemplateWithBodyData
    {
        $data = new MailingTemplateWithBodyData();
        $data->id = (int) $row['mailing_template_id'];
        $data->panelId = (int) $row['panel_id'];
        $data->senderId = (int) $row['sender_id'];
        $data->title = $row['title'];
        $data->subject = $row['subject'];
        $data->replyTo = $row['reply_to'];
        $data->bodyText = $row['body_text'];
        $data->bodyHtml = $row['body_html'];

        return $data;
    }

    /**
     * @param array $row
     *
     * @return MailingTemplateFullData
     */
    private function buildFullData(array $row): MailingTemplateFullData
    {
        $data = new MailingTemplateFullData();
        $data->id = (int) $row['mailing_template_id'];
        $data->panelId = (int) $row['panel_id'];
        $data->senderId = (int) $row['sender_id'];
        $data->title = $row['title'];
        $data->subject = $row['subject'];
        $data->replyTo = $row['reply_to'];
        $data->bodyText = $row['body_text'];
        $data->bodyHtml = $row['body_html'];
        $data->sender = new MailingSenderData();
        $data->sender->name = $row['name'];
        $data->sender->email = $row['email'];
        $data->sender->id = (int) $row['sender_id'];

        return $data;
    }
}
