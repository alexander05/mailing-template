<?php
namespace Main\Dashboard\Repository;

use Main\Dashboard\DTO\MailingFilterData;
use Main\Dashboard\DTO\MailingTemplateData;
use Main\Dashboard\DTO\MailingTemplateFullData;
use Main\Dashboard\DTO\MailingTemplateWithBodyData;
use Main\Dashboard\DTO\SortOrderData;

interface MailingTemplateRepository
{
    /**
     * @return MailingTemplateData[]
     */
    public function all(): array;

    /**
     * @param int $id
     *
     * @return MailingTemplateWithBodyData|null
     */
    public function findById(int $id): ?MailingTemplateWithBodyData;

    /**
     * @param MailingTemplateWithBodyData $data
     *
     * @return MailingTemplateWithBodyData
     */
    public function create(MailingTemplateWithBodyData $data): MailingTemplateWithBodyData;

    /**
     * @param MailingTemplateWithBodyData $data
     *
     * @return bool
     */
    public function update(MailingTemplateWithBodyData $data): bool;

    /**
     * @param int $id
     */
    public function remove(int $id);

    /**
     * @param MailingFilterData $mailingFilterData
     * @param SortOrderData     $sortOrderData
     *
     * @return int[]
     */
    public function filter(MailingFilterData $mailingFilterData, SortOrderData $sortOrderData): array;

    /**
     * @param int[] $ids
     *
     * @return MailingTemplateFullData[]
     */
    public function listFullDataByIds(array $ids): array;
}
