<?php
namespace Main\Dashboard\Service\Manager;

use AppBundle\Security\AuthorizationCheckerTrait;
use AppBundle\Security\Role\Permissions;
use Main\Dashboard\DTO\MailingFilterData;
use Main\Dashboard\DTO\MailingTemplateWithBodyData;
use Main\Dashboard\DTO\SortOrderData;
use Main\Dashboard\Exception\ObjectNotFoundException;
use Main\Dashboard\Repository\MailingTemplateRepository;
use Main\Dashboard\Service\MailingTemplateService;
use Main\Validation\ValidatorAwareTrait;
use Html2Text\Html2Text;

class MailingTemplateManager implements MailingTemplateService
{
    use AuthorizationCheckerTrait;
    use ValidatorAwareTrait;

    /** @var MailingTemplateRepository */
    private $repository;

    /**
     * @param MailingTemplateRepository $repository
     */
    public function __construct(MailingTemplateRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function getById(int $id): MailingTemplateWithBodyData
    {
        $this->denyAccessUnlessGranted(Permissions::VIEW_MAILING_TEMPLATE, $id);

        $mailingTemplate = $this->repository->findById($id);

        if (is_null($mailingTemplate)) {
            throw new ObjectNotFoundException(ObjectNotFoundException::MAILING_TEMPLATE_OBJECT, ['id' => $id]);
        }

        return $mailingTemplate;
    }

    /**
     * {@inheritdoc}
     */
    public function list(): array
    {
        $this->denyAccessUnlessGranted(Permissions::LIST_MAILING_TEMPLATE);

        return $this->repository->all();
    }

    /**
     * {@inheritdoc}
     */
    public function listFullDataByIds(array $ids): array
    {
        return $this->repository->listFullDataByIds($ids);
    }

    /**
     * {@inheritdoc}
     */
    public function create(MailingTemplateWithBodyData $data): MailingTemplateWithBodyData
    {
        $this->denyAccessUnlessGranted(Permissions::CREATE_MAILING_TEMPLATE);
        $this->validate($data);

        return $this->repository->create($data);
    }

    /**
     * {@inheritdoc}
     */
    public function update(int $id, MailingTemplateWithBodyData $data): MailingTemplateWithBodyData
    {
        $this->denyAccessUnlessGranted(Permissions::EDIT_MAILING_TEMPLATE, $id);

        $mailingTemplateWithBodyData = $this->getById($id);

        if (null !== $data->panelId) {
            $mailingTemplateWithBodyData->panelId = $data->panelId;
        }

        if (null !== $data->senderId) {
            $mailingTemplateWithBodyData->senderId = $data->senderId;
        }

        if (null !== $data->title) {
            $mailingTemplateWithBodyData->title = $data->title;
        }

        if (null !== $data->subject) {
            $mailingTemplateWithBodyData->subject = $data->subject;
        }

        if (null !== $data->bodyText) {
            $mailingTemplateWithBodyData->bodyText = $data->bodyText;
        }
        if (null !== $data->bodyHtml) {
            $mailingTemplateWithBodyData->bodyHtml = $data->bodyHtml;
        }

        if (null !== $data->replyTo) {
            $mailingTemplateWithBodyData->replyTo = $data->replyTo;
        }

        $this->validate($mailingTemplateWithBodyData);
        $this->repository->update($mailingTemplateWithBodyData);

        return $mailingTemplateWithBodyData;
    }

    /**
     * {@inheritdoc}
     */
    public function remove(int $id)
    {
        $this->denyAccessUnlessGranted(Permissions::REMOVE_MAILING_TEMPLATE, $id);

        if ($this->getById($id)) {
            $this->repository->remove($id);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function filter(MailingFilterData $mailingFilterData, SortOrderData $sortOrderData): array
    {
        return $this->repository->filter($mailingFilterData, $sortOrderData);
    }

    /**
     * {@inheritdoc}
     */
    public function html2txt(string $html): string
    {
        return Html2Text::convert($html);
    }

    /**
     * {@inheritdoc}
     */
    public function txt2html(string $text): string
    {
        return str_replace(["\r", "\n"], '', nl2br($text));
    }
}
