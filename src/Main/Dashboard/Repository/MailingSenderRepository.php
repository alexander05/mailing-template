<?php
namespace Main\Dashboard\Repository;

use Main\Dashboard\DTO\MailingSenderData;

interface MailingSenderRepository
{
    /**
     * @param int $id
     *
     * @return MailingSenderData|null
     */
    public function findById(int $id): ?MailingSenderData;

    /**
     * @return MailingSenderData[]
     */
    public function all(): array;

    /**
     * @param MailingSenderData $data
     *
     * @return MailingSenderData
     */
    public function create(MailingSenderData $data): MailingSenderData;

    /**
     * @param MailingSenderData $data
     *
     * @return bool
     */
    public function update(MailingSenderData $data): bool;

    /**
     * @param int $id
     */
    public function remove(int $id);
}
