<?php
namespace Main\Dashboard\Service;

use Main\Dashboard\DTO\MailingSenderData;
use Main\Dashboard\Exception\InvalidSenderEmailException;
use Main\Dashboard\Exception\ObjectNotFoundException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

interface MailingSenderService
{
    /**
     * @param int $id
     *
     * @return MailingSenderData
     *
     * @throws ObjectNotFoundException
     */
    public function getById(int $id): MailingSenderData;

    /**
     * @return MailingSenderData[]
     *
     * @throws AccessDeniedException
     */
    public function list(): array;

    /**
     * @param MailingSenderData $data
     *
     * @return MailingSenderData
     */
    public function create(MailingSenderData $data): MailingSenderData;

    /**
     * @param int               $id
     * @param MailingSenderData $data
     *
     * @return MailingSenderData
     *
     * @throws ObjectNotFoundException
     */
    public function update(int $id, MailingSenderData $data): MailingSenderData;

    /**
     * @param int $id
     */
    public function remove(int $id);

    /**
     * @param string $email
     *
     * @return bool
     */
    public function isValidEmail(string $email): bool;

    /**
     * @param MailingSenderData[] $senders
     *
     * @throws InvalidSenderEmailException
     */
    public function checkValidEmailsBySenders(array $senders): void;
}
