<?php

class UpdateMailingTemplateCest
{
    public function _before(ApiTester $i)
    {
        $i->loadFixtures('MailingTemplate');
    }

    /**
     * @param ApiTester $i
     */
    public function tryToUpdateNotAuthorized(ApiTester $i)
    {
        $i->amAuthenticatedByApiKey('list-all-mailing-template@mail.ru', '12345');
        $i->wantTo('get error when update mailing update via API from /mailing/template/{id}/update');
        $i->testApiRequest(
            '/mailing/template/3/update',
            [
                'payload' => [
                    'panelId'=> 1,
                    'senderId'=> 1,
                    'title' => 'new test mailing template text',
                    'subject'=> 'subject1',
                    'replyTo'=> 'reply1',
                    'bodyText' => 'bodytext1',
                    'bodyHtml' => 'bodyhtml1'
                ],
            ],
            [
                'result' => 'error',
                'error' => 'ACCESS-DENIED',
                'data' => [
                    'attributes' => ['EDIT_MAILING_TEMPLATE'],
                    'subject' => 3,
                ],
            ]
        );
    }

    /**
     * @param ApiTester $i
     */
    public function tryToUpdateNonExistentMailingTemplate(ApiTester $i)
    {
        $i->amAuthenticatedByApiKey('has-role-manage-mailing-template@mail.ru', '12345');
        $i->wantTo('get error when update mailing template via API from /mailing/template/{id}/update');
        $i->testApiRequest(
            '/mailing/template/999/update',
            [
                'payload' => [
                    'panelId'=> 1,
                    'senderId'=> 1,
                    'title' => 'new test mailing template text',
                    'subject'=> 'subject1',
                    'replyTo'=> 'reply1',
                    'bodyText' => 'bodytext1',
                    'bodyHtml' => 'bodyhtml1'
                ],
            ],
            [
                'result' => 'error',
                'error' => 'OBJECT-NOT-FOUND',
                'data' => [
                    'type' => 'mailing_template',
                    'identity' => ['id' => '999'],
                ],
            ]
        );
    }

    /**
     * @param ApiTester $i
     */
    public function tryToUpdateIncorrectData(ApiTester $i)
    {
        $i->wantTo('get validation error on update mailing template with incorrect input data via API from /mailing/template/{id}/update');
        $i->amAuthenticatedByApiKey('has-role-manage-mailing-template@mail.ru', '12345');
        $i->testApiRequest(
            '/mailing/template/3/update',
            [
                'payload' => [
                    'panelId'=> 999,
                    'senderId'=> 999,
                    'title' => '',
                    'subject'=> '',
                    'replyTo'=> '',
                    'bodyText' => '',
                    'bodyHtml' => ''
                ],
            ],
            [
                'result' => 'error',
                'error' => 'VALIDATION-VIOLATION',
                'data' => [
                    [
                        'property_path' => 'title',
                        'message' => 'This value should not be blank.',
                    ],
                    [
                        'property_path' => 'panelId',
                        'message' => 'PanelId - should be id of existing panel. "999" doesn\'t exist',
                    ],
                    [
                        'property_path' => 'senderId',
                        'message' => 'SenderId - should be id of existing sender. "999" doesn\'t exist',
                    ],
                    [
                        'property_path' => 'replyTo',
                        'message' => 'This value should not be blank.',
                    ],
                ],
            ]
        );
    }

    /**
     * @param ApiTester $i
     */
    public function tryToUpdate(ApiTester $i)
    {
        $i->wantTo('update mailing template via API from /mailing/template/{id}/update');
        $i->amAuthenticatedByApiKey('has-role-manage-mailing-template@mail.ru', '12345');
        $i->testApiRequest(
            '/mailing/template/3/update',
            [
                'payload' => [
                    'title' => 'new test mailing template text',
                    'bodyText' => 'new bodytext',
                ],
            ],
            [
                'result' => 'success',
                'data' => [
                    'id'=> 3,
                    'panelId'=> 1,
                    'senderId'=> 1,
                    'title' => 'new test mailing template text',
                    'subject'=> 'subject3',
                    'replyTo'=> 'reply3',
                    'bodyText' => 'new bodytext',
                    'bodyHtml' => 'bodyhtml3'
                ],
            ]
        );

        $i->seeInDatabase('mailing_template', [
            'mailing_template_id' => 3,
            'title' => 'new test mailing template text',
        ]);
    }
}
