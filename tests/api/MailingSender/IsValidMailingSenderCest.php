<?php

class IsValidMailingSenderCest
{
    public function _before(ApiTester $i)
    {
        $i->loadFixtures('MailingSender');
    }

    /**
     * @param ApiTester $i
     *
     */
    public function tryToIsValidMailingSender(ApiTester $i)
    {
        $i->amAuthenticatedByApiKey('list-all-mailing-sender@mail.ru', '12345');
        $i->wantTo('check email mailing sender is valid  API from /mailing/sender/is_valid');

        $i->mockExternalRequests(
            'mailer_api_client.emails_service',
            [
                'emails' => [
                    'result' => 'success',
                    'data' => ['something@mail.ru'],
                ],
            ]
        );

        $i->testApiRequest(
            '/mailing/sender/is_valid',
            [
                'payload' => [
                    'email' => 'something@mail.ru',
                ],
            ],
            [
                'result' => 'success',
                'data'  => true
            ]
        );
    }

    /**
     * @param ApiTester $i
     *
     */
    public function tryToIsNotValidMailingSender(ApiTester $i)
    {
        $i->amAuthenticatedByApiKey('list-all-mailing-sender@mail.ru', '12345');
        $i->wantTo('check email mailing sender is not valid  API from /mailing/sender/is_valid');

        $i->mockExternalRequests(
            'mailer_api_client.emails_service',
            [
                'emails' => [
                    'result' => 'success',
                    'data' => ['something@mail.ru'],
                ],
            ]
        );

        $i->testApiRequest(
            '/mailing/sender/is_valid',
            [
                'payload' => [
                    'email' => 'other_email@mail.ru',
                ],
            ],
            [
                'result' => 'success',
                'data'  => false
            ]
        );
    }
}
