<?php

class Txt2HtmlMailingTemplateCest
{
    public function _before(ApiTester $i)
    {
        $i->loadFixtures('MailingTemplate');
    }

    /**
     * @param ApiTester $i
     *
     */
    public function tryToConvertTxt2Html(ApiTester $i)
    {
        $i->amAuthenticatedByApiKey('list-all-mailing-template@mail.ru', '12345');
        $i->wantTo('convert txt to html via API from /mailing/template/txt2html');
        $i->testApiRequest(
            '/mailing/template/txt2html',
            [
                'payload' => [
                    'data'=> "The line\nThe other line",
                ],
            ],
            [
                'result' => 'success',
                'data' => "The line<br />The other line",
            ]
        );

    }

}