<?php
namespace Main\Dashboard\DTO;

class MailingFilterData
{
    /** @var string */
    public $title;

    /** @var int */
    public $senderId;

    /** @var string */
    public $senderEmail;

    /** @var int */
    public $panelId;
}
