<?php
namespace Main\Dashboard\Exception;

use Throwable;

/**
 * Exception for mailing sender errors.
 */
class MailingSenderException extends AppException
{
    const REASON_INVALID_REQUEST = 'INVALID-REQUEST';

    /** @var string */
    private $reason;

    /**
     * @param string         $reason
     * @param Throwable|null $previous
     */
    public function __construct(string $reason, Throwable $previous = null)
    {
        $this->reason = $reason;
        parent::__construct($reason, null, sprintf('Mailing sender error: "%s"', $this->reason), 0, $previous);
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }
}
