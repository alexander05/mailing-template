<?php

class RemoveMailingSenderCest
{
    public function _before(ApiTester $i)
    {
        $i->loadFixtures('MailingSender');
    }

    /**
     * @param ApiTester $i
     */
    public function tryToRemoveNotAuthorized(ApiTester $i)
    {
        $i->amAuthenticatedByApiKey('list-all-mailing-sender@mail.ru', '12345');
        $i->wantTo('get access deniend error when removing mailing sender via API from /mailing/sender/{id}/remove');
        $i->testApiRequest(
            '/mailing/sender/1/remove',
            [],
            [
                'result' => 'error',
                'error' => 'ACCESS-DENIED',
                'data' => [
                    'attributes' => ['REMOVE_MAILING_SENDER'],
                    'subject' => 1,
                ],
            ]
        );
    }

    /**
     * @param ApiTester $i
     */
    public function tryToRemoveNonExistentMailingSender(ApiTester $i)
    {
        $i->amAuthenticatedByApiKey('has-role-manage-mailing-sender@mail.ru', '12345');
        $i->wantTo('get error when removing non-existent mailing sender via API from /mailing/sender/{id}/remove');
        $i->testApiRequest(
            '/mailing/sender/999/remove',
            [],
            [
                'result' => 'error',
                'error' => 'OBJECT-NOT-FOUND',
                'data' => [
                    'type' => 'mailing_sender',
                    'identity' => [
                        'id' => 999
                    ],
                ],
            ]
        );
    }

    /**
     * @param ApiTester $i
     */
    public function tryToRemoveSuccess(ApiTester $i)
    {
        $i->amAuthenticatedByApiKey('has-role-manage-mailing-sender@mail.ru', '12345');
        $i->wantTo('remove mailing sender via API from /mailing/sender/{id}/remove');
        $i->testApiRequest(
            '/mailing/sender/1/remove',
            [],
            [
                'result' => 'success',
            ]
        );

        $i->dontSeeInDatabase('mailing_sender', [
            'sender_id' => 1,
            'name' => 'First',
        ]);
    }
}
