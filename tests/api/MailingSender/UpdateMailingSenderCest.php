<?php

class UpdateMailingSenderCest
{
    public function _before(ApiTester $i)
    {
        $i->loadFixtures('MailingSender');
    }

    /**
     * @param ApiTester $i
     */
    public function tryToUpdateNotAuthorized(ApiTester $i)
    {
        $i->amAuthenticatedByApiKey('list-all-mailing-sender@mail.ru', '12345');
        $i->wantTo('get error when update mailing update via API from /mailing/sender/{id}/update');
        $i->testApiRequest(
            '/mailing/sender/3/update',
            [
                'payload' => [
                    'email' => 'Some@mail.ru',
                    'name' => 'SomeName'
                ],
            ],
            [
                'result' => 'error',
                'error' => 'ACCESS-DENIED',
                'data' => [
                    'attributes' => ['EDIT_MAILING_SENDER'],
                    'subject' => 3,
                ],
            ]
        );
    }

    /**
     * @param ApiTester $i
     */
    public function tryToUpdateNonExistentMailingSender(ApiTester $i)
    {
        $i->amAuthenticatedByApiKey('has-role-manage-mailing-sender@mail.ru', '12345');
        $i->wantTo('get error when update mailing sender via API from /mailing/sender/{id}/update');
        $i->testApiRequest(
            '/mailing/sender/999/update',
            [
                'payload' => [
                    'panelId'=> 1,
                    'senderId'=> 1,
                    'title' => 'new test mailing sender text',
                    'subject'=> 'subject1',
                    'replyTo'=> 'reply1',
                    'bodyText' => 'bodytext1',
                    'bodyHtml' => 'bodyhtml1'
                ],
            ],
            [
                'result' => 'error',
                'error' => 'OBJECT-NOT-FOUND',
                'data' => [
                    'type' => 'mailing_sender',
                    'identity' => ['id' => '999'],
                ],
            ]
        );
    }

    /**
     * @param ApiTester $i
     */
    public function tryToUpdateIncorrectData(ApiTester $i)
    {
        $i->wantTo('get validation error on update mailing sender with incorrect input data via API from /mailing/sender/{id}/update');
        $i->amAuthenticatedByApiKey('has-role-manage-mailing-sender@mail.ru', '12345');

        $i->testApiRequest(
            '/mailing/sender/3/update',
            [
                'payload' => [
                    'email' => 'NewMailmail.ru',
                    'name' => ''
                ],
            ],
            [
                'result' => 'error',
                'error' => 'VALIDATION-VIOLATION',
                'data' => [
                    [
                        'property_path' => 'email',
                        'message' => 'Email - should be valid.',
                    ],
                    [
                        'property_path' => 'name',
                        'message' => 'This value should not be blank.',
                    ],
                ],
            ]
        );
    }

    /**
     * @param ApiTester $i
     */
    public function tryToUpdate(ApiTester $i)
    {
        $i->wantTo('update mailing sender via API from /mailing/sender/{id}/update');
        $i->amAuthenticatedByApiKey('has-role-manage-mailing-sender@mail.ru', '12345');
        $i->testApiRequest(
            '/mailing/sender/3/update',
            [
                'payload' => [
                    'name' => 'NewName'
                ],
            ],
            [
                'result' => 'success',
                'data' => [
                    'id'=> 3,
                    'email' => 'Third@mail.ru',
                    'name' => 'NewName'
                ],
            ]
        );

        $i->seeInDatabase('mailing_sender', [
            'sender_id' => 3,
            'name' => 'NewName',
        ]);
    }
}