<?php
namespace Main\Dashboard\DTO;

class MailingSenderData
{
    /** @var int */
    public $id;

    /** @var string */
    public $email;

    /** @var string */
    public $name;
}
