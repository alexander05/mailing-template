<?php
namespace AppBundle\Controller;

use Main\Dashboard\DTO\MailingSenderData;
use Main\Dashboard\Service\MailingSenderService;
use Symfony\Component\HttpFoundation\Request;
use Main\Dashboard\Exception\MailingSenderException;
use Symfony\Component\HttpFoundation\ParameterBag;

class MailingSenderController
{
    /** @var MailingSenderService */
    private $service;

    /**
     * @param MailingSenderService $service
     */
    public function __construct(MailingSenderService $service)
    {
        $this->service = $service;
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function isValidAction(Request $request): bool
    {
        $payload = $this->getRequestAttribute($request, 'payload');

        return $this->service->isValidEmail($payload->get('email', ''));
    }

    /**
     * @param int $id
     *
     * @return MailingSenderData
     */
    public function getAction(int $id): MailingSenderData
    {
        return $this->service->getById($id);
    }

    /**
     * @return MailingSenderData[]
     */
    public function allAction(): array
    {
        return $this->service->list();
    }

    /**
     * @param Request $request
     *
     * @return MailingSenderData
     */
    public function createAction(Request $request): MailingSenderData
    {
        $payload = $this->getRequestAttribute($request, 'payload');

        $data = $this->buildData($payload);
        $result = $this->service->create($data);

        return $result;
    }

    /**
     * @param int     $id
     * @param Request $request
     *
     * @return MailingSenderData
     */
    public function updateAction(int $id, Request $request): MailingSenderData
    {
        $payload = $this->getRequestAttribute($request, 'payload');

        $data = $this->buildData($payload);

        return $this->service->update($id, $data);
    }


    /**
     * @param int $id
     */
    public function removeAction(int $id)
    {
        $this->service->remove($id);
    }

    /**
     * @param ParameterBag $payload
     *
     * @return MailingSenderData
     */
    private function buildData(ParameterBag $payload): MailingSenderData
    {
        $data = new MailingSenderData();
        $data->id = $payload->get('id');
        $data->email = $payload->get('email');
        $data->name = $payload->get('name');

        return $data;
    }

    /**
     * @param Request $request
     * @param string  $attribute
     *
     * @return ParameterBag
     *
     * @throws MailingSenderException
     */
    private function getRequestAttribute(Request $request, string $attribute): ParameterBag
    {
        $value = $request->attributes->get($attribute);

        if (empty($value)) {
            throw new MailingSenderException(MailingSenderException::REASON_INVALID_REQUEST);
        }

        return $value;
    }
}
