<?php
namespace Main\Dashboard\Service;

use Main\Dashboard\DTO\MailingFilterData;
use Main\Dashboard\DTO\MailingTemplateData;
use Main\Dashboard\DTO\MailingTemplateFullData;
use Main\Dashboard\DTO\MailingTemplateWithBodyData;
use Main\Dashboard\DTO\SortOrderData;
use Main\Dashboard\Exception\ObjectNotFoundException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

interface MailingTemplateService
{
    /**
     * @param int $id
     *
     * @return MailingTemplateWithBodyData
     *
     * @throws ObjectNotFoundException
     */
    public function getById(int $id): MailingTemplateWithBodyData;

    /**
     * @return MailingTemplateData[]
     *
     * @throws AccessDeniedException
     */
    public function list(): array;

    /**
     * @param int[] $ids
     *
     * @return MailingTemplateFullData[]
     */
    public function listFullDataByIds(array $ids): array;

    /**
     * @param MailingTemplateWithBodyData $data
     *
     * @return MailingTemplateWithBodyData
     */
    public function create(MailingTemplateWithBodyData $data): MailingTemplateWithBodyData;

    /**
     * @param int                         $id
     * @param MailingTemplateWithBodyData $data
     *
     * @return MailingTemplateWithBodyData
     *
     * @throws ObjectNotFoundException
     */
    public function update(int $id, MailingTemplateWithBodyData $data): MailingTemplateWithBodyData;

    /**
     * @param int $id
     */
    public function remove(int $id);

    /**
     * @param MailingFilterData $mailingFilterData
     * @param SortOrderData     $sortOrderData
     *
     * @return int[]
     */
    public function filter(MailingFilterData $mailingFilterData, SortOrderData $sortOrderData): array;

    /**
     * @param string $html
     *
     * @return string
     */
    public function html2txt(string $html): string;

    /**
     * @param string $text
     *
     * @return string
     */
    public function txt2html(string $text): string;
}
