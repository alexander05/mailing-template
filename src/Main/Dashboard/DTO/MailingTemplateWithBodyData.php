<?php
namespace Main\Dashboard\DTO;

class MailingTemplateWithBodyData extends MailingTemplateData
{
    /** @var string */
    public $bodyText;

    /** @var string */
    public $bodyHtml;
}
