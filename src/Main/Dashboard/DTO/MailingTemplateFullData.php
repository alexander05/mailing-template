<?php
namespace Main\Dashboard\DTO;

class MailingTemplateFullData extends MailingTemplateWithBodyData
{
    /** @var MailingSenderData */
    public $sender;
}
