<?php
namespace Main\Dashboard\Fixtures;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Main\Dashboard\DTO\MailingTemplateWithBodyData;
use Main\Dashboard\Repository\Doctrine\MailingTemplateDoctrineRepository;
use Main\FixturesBundle\Exception\FixtureLoadException;
use Main\FixturesBundle\FixtureLoader\FixtureLoader;

class MailingTemplateFixtureLoader extends FixtureLoader
{
    /** @var Connection */
    private $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        parent::__construct();
        $this->connection = $connection;
    }

    /**
     * {@inheritdoc}
     */
    public function getSupportedClass(): string
    {
        return MailingTemplateWithBodyData::class;
    }

    /**
     * {@inheritdoc}
     */
    protected function insert($object)
    {
        try {
            $data = [
                'panel_id' => $object->panelId,
                'sender_id' => $object->senderId,
                'title' => $object->title,
                'subject' => $object->subject,
                'reply_to' => $object->replyTo,
                'body_text' => $object->bodyText,
                'body_html' => $object->bodyHtml,
            ];

            if ($object->id) {
                $data['mailing_template_id'] = (int) $object->id;
            }

            $this->connection->insert(MailingTemplateDoctrineRepository::TABLE, $data);

            if ($object->id) {
                $nextval = $this->connection->fetchColumn("select nextval('mailing_template_mailing_template_id_seq')")[0];
                if ($nextval <= $object->id) {
                    $this->connection->executeQuery("select setval('mailing_template_mailing_template_id_seq', :value)", ['value' => $object->id + 1]);
                }
            } else {
                $object->id = $this->connection->lastInsertId();
            }

            return $object->id;
        } catch (DBALException $e) {
            throw new FixtureLoadException(sprintf('Fixture insert failed. Error: %s', $e->getMessage()), 0, $e);
        }
    }
}
