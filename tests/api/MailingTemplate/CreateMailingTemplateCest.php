
<?php

class CreateMailingTemplateCest
{
    public function _before(ApiTester $i)
    {
        $i->loadFixtures('MailingTemplate');
    }

    /**
     * @param ApiTester $i
     */
    public function tryToCreateNotAuthorized(ApiTester $i)
    {
        $i->amAuthenticatedByApiKey('list-all-mailing-template@mail.ru', '12345');
        $i->wantTo('get error when create mailing template via API from /mailing/template/create');
        $i->testApiRequest(
            '/mailing/template/create',
            [
                'payload' => [
                    'panelId'=> 2,
                    'senderId'=> 2,
                    'title' => 'test mailing template',
                    'subject'=> 'subject4',
                    'replyTo'=> 'reply4',
                    'bodyText' => 'bodytext4',
                    'bodyHtml' => 'bodyhtml4'
                ],
            ],
            [
                'result' => 'error',
                'error' => 'ACCESS-DENIED',
                'data' => [
                    'attributes' => ['CREATE_MAILING_TEMPLATE'],
                    'subject' => null,
                ],
            ]
        );
    }

    /**
     * @param ApiTester $i
     */
    public function tryToCreateIncorrectData(ApiTester $i)
    {
        $i->wantTo('get validation error on create mailing template with incorrect input data via API from /mailing/template/create');
        $i->amAuthenticatedByApiKey('has-role-manage-mailing-template@mail.ru', '12345');

        $i->testApiRequest(
            '/mailing/template/create',
            [
                'payload' => [
                    'panelId'=> 999,
                    'senderId'=> 999,
                    'title' => '',
                    'subject'=> '',
                    'replyTo'=> '',
                    'bodyText' => '',
                    'bodyHtml' => ''
                ],
            ],
            [
                'result' => 'error',
                'error' => 'VALIDATION-VIOLATION',
                'data' => [
                    [
                        'property_path' => 'title',
                        'message' => 'This value should not be blank.',
                    ],
                    [
                        'property_path' => 'panelId',
                        'message' => 'PanelId - should be id of existing panel. "999" doesn\'t exist',
                    ],
                    [
                        'property_path' => 'senderId',
                        'message' => 'SenderId - should be id of existing sender. "999" doesn\'t exist',
                    ],
                    [
                        'property_path' => 'replyTo',
                        'message' => 'This value should not be blank.',
                    ],
                ],
            ]
        );
    }

    /**
     * @param ApiTester $i
     */
    public function tryToCreate(ApiTester $i)
    {
        $i->wantTo('create mailing template via API from /mailing/template/create');
        $i->amAuthenticatedByApiKey('has-role-manage-mailing-template@mail.ru', '12345');

        $i->testApiRequest(
            '/mailing/template/create',
            [
                'payload' => [
                    'panelId'=> 1,
                    'senderId'=> 1,
                    'title' => 'test mailing template',
                    'subject'=> 'subject4',
                    'replyTo'=> 'reply4',
                    'bodyText' => 'bodytext4',
                    'bodyHtml' => 'bodyhtml4'
                ],
            ],
            [
                'result' => 'success',
                'data' => [
                    'panelId'=> 1,
                    'senderId'=> 1,
                    'title' => 'test mailing template',
                    'subject'=> 'subject4',
                    'replyTo'=> 'reply4',
                    'bodyText' => 'bodytext4',
                    'bodyHtml' => 'bodyhtml4'
                ],
            ]
        );


        $id = $i->grabDataFromResponseByJsonPath('$.data.id')[0];
        $i->seeInDatabase('mailing_template', [
            'mailing_template_id' => $id,
            'title' => 'test mailing template',
        ]);
    }
}