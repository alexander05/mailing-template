<?php
namespace AppBundle\Controller;

use Main\Dashboard\DTO\MailingFilterData;
use Main\Dashboard\DTO\MailingTemplateData;
use Main\Dashboard\DTO\MailingTemplateWithBodyData;
use Main\Dashboard\DTO\SortOrderData;
use Main\Dashboard\Service\MailingTemplateService;
use Symfony\Component\HttpFoundation\Request;
use Main\Dashboard\Exception\MailingTemplateException;
use Symfony\Component\HttpFoundation\ParameterBag;
use Main\Dashboard\Exception\ObjectNotFoundException;

class MailingTemplateController
{
    /** @var MailingTemplateService */
    private $service;

    /**
     * @param MailingTemplateService $service
     */
    public function __construct(MailingTemplateService $service)
    {
        $this->service = $service;
    }

    /**
     * @param int $id
     *
     * @return MailingTemplateWithBodyData
     */
    public function getAction(int $id): MailingTemplateWithBodyData
    {
        return $this->service->getById($id);
    }

    /**
     * @return MailingTemplateData[]
     */
    public function listAction(): array
    {
        return $this->service->list();
    }

    /**
     * @param Request $request
     *
     * @return int[]
     *
     * @throws ObjectNotFoundException
     */
    public function filterAction(Request $request): array
    {
        $payload = $this->getPayload($request);
        $filterData = $this->buildMailingFilterData($payload);
        $sortOrderData = $this->buildSortOrderData($payload);

        return $this->service->filter($filterData, $sortOrderData);
    }

    /**
     * @param Request $request
     *
     * @return MailingTemplateWithBodyData
     */
    public function createAction(Request $request): MailingTemplateWithBodyData
    {
        $data = $this->buildData($this->getPayload($request));
        $result = $this->service->create($data);

        return $result;
    }

    /**
     * @param int     $id
     * @param Request $request
     *
     * @return MailingTemplateWithBodyData
     */
    public function updateAction(int $id, Request $request): MailingTemplateWithBodyData
    {
        $data = $this->buildData($this->getPayload($request));

        return $this->service->update($id, $data);
    }


    /**
     * @param int     $id
     * @param Request $request
     */
    public function removeAction(int $id, Request $request)
    {
        $this->service->remove($id);
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    public function txt2htmlAction(Request $request): string
    {
        return $this->service->txt2html($this->getPayload($request)->get('data', ''));
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    public function html2txtAction(Request $request): string
    {
        return $this->service->html2txt($this->getPayload($request)->get('data', ''));
    }

    /**
     * @param Request $request
     *
     * @return ParameterBag
     */
    private function getPayload(Request $request): ParameterBag
    {
        return $this->getRequestAttribute($request, 'payload');
    }

    /**
     * @param ParameterBag $payload
     *
     * @return MailingTemplateWithBodyData
     */
    private function buildData(ParameterBag $payload): MailingTemplateWithBodyData
    {
        $data = new MailingTemplateWithBodyData();
        $data->id = $payload->get('id');
        $data->panelId = $payload->get('panelId');
        $data->senderId = $payload->get('senderId');
        $data->title = $payload->get('title');
        $data->subject = $payload->get('subject');
        $data->bodyText = $payload->get('bodyText');
        $data->bodyHtml = $payload->get('bodyHtml');
        $data->replyTo = $payload->get('replyTo');

        return $data;
    }

    /**
     * @param ParameterBag $payload
     *
     * @return MailingFilterData
     */
    private function buildMailingFilterData(ParameterBag $payload): MailingFilterData
    {
        $filter = $payload->get('filter', []);

        $data = new MailingFilterData();
        $data->title = $filter['title'] ?? null;
        $data->senderId = $filter['senderId'] ?? null;
        $data->senderEmail = $filter['senderEmail'] ?? null;
        $data->panelId = $filter['panelId'] ?? null;

        return $data;
    }

    /**
     * @param ParameterBag $payload
     *
     * @return SortOrderData
     */
    private function buildSortOrderData(ParameterBag $payload): SortOrderData
    {
        $orderBy = $payload->get('orderBy', []);

        $data = new SortOrderData();
        $data->field = $orderBy['field'] ?? null;
        $data->order = $orderBy['order'] ?? null;

        return $data;
    }

    /**
     * @param Request $request
     * @param string $attribute
     *
     * @return ParameterBag
     *
     * @throws MailingTemplateException
     */
    private function getRequestAttribute(Request $request, string $attribute): ParameterBag
    {
        $value = $request->attributes->get($attribute);

        if (empty($value)) {
            throw new MailingTemplateException(MailingTemplateException::REASON_INVALID_REQUEST);
        }

        return $value;
    }
}
