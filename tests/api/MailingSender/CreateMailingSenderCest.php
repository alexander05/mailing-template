<?php

class CreateMailingSenderCest
{
    public function _before(ApiTester $i)
    {
        $i->loadFixtures('MailingSender');
    }

    /**
     * @param ApiTester $i
     */
    public function tryToCreateNotAuthorized(ApiTester $i)
    {
        $i->amAuthenticatedByApiKey('list-all-mailing-sender@mail.ru', '12345');
        $i->wantTo('get error when create mailing sender via API from /mailing/sender/create');
        $i->testApiRequest(
            '/mailing/sender/create',
            [
                'payload' => [
                    'email' => 'NewMail@mail.ru',
                    'name' => 'NewName'
                ],
            ],
            [
                'result' => 'error',
                'error' => 'ACCESS-DENIED',
                'data' => [
                    'attributes' => ['CREATE_MAILING_SENDER'],
                    'subject' => null,
                ],
            ]
        );
    }

    /**
     * @param ApiTester $i
     */
    public function tryToCreateIncorrectData(ApiTester $i)
    {
        $i->wantTo('get validation error on create mailing sender with incorrect input data via API from /mailing/sender/create');
        $i->amAuthenticatedByApiKey('has-role-manage-mailing-sender@mail.ru', '12345');

        $i->testApiRequest(
            '/mailing/sender/create',
            [
                'payload' => [
                    'email' => 'NewMailmail.ru',
                    'name' => ''
                ],
            ],
            [
                'result' => 'error',
                'error' => 'VALIDATION-VIOLATION',
                'data' => [
                    [
                        'property_path' => 'email',
                        'message' => 'Email - should be valid.',
                    ],
                    [
                        'property_path' => 'name',
                        'message' => 'This value should not be blank.',
                    ],
                ],
            ]
        );
    }

    /**
     * @param ApiTester $i
     */
    public function tryToCreate(ApiTester $i)
    {
        $i->wantTo('create mailing sender via API from /mailing/sender/create');
        $i->amAuthenticatedByApiKey('has-role-manage-mailing-sender@mail.ru', '12345');

        $i->testApiRequest(
            '/mailing/sender/create',
            [
                'payload' => [
                    'email' => 'NewMail@mail.ru',
                    'name' => 'NewName'
                ],
            ],
            [
                'result' => 'success',
                'data' => [
                    'email' => 'NewMail@mail.ru',
                    'name' => 'NewName'
                ],
            ]
        );


        $id = $i->grabDataFromResponseByJsonPath('$.data.id')[0];
        $i->seeInDatabase('mailing_sender', [
            'sender_id' => $id,
            'email' => 'NewMail@mail.ru',
        ]);
    }

}