<?php

class MailingSenderCest
{
    public function _before(ApiTester $i)
    {
        $i->loadFixtures('MailingSender');
    }

    /**
     * @param ApiTester $i
     *
     */
    public function tryToListMailingSender(ApiTester $i)
    {
        $i->amAuthenticatedByApiKey('has-role-manage-mailing-sender@mail.ru', '12345');
        $i->wantTo('get mailing sender list via API from /mailing/sender/all');

        $i->testApiRequest(
            '/mailing/sender/all',
            [],
            ['result' => 'success']
        );

        $data = $i->grabDataFromResponseByJsonPath('$.data[*]');
        $i->assertEquals([
            [
                'id' => 1,
                'email'=> 'First@mail.ru',
                'name'=> 'First',
            ],
            [
                'id' => 2,
                'email'=> 'Second@mail.ru',
                'name'=> 'Second',
            ],
            [
                'id' => 3,
                'email'=> 'Third@mail.ru',
                'name'=> 'Third',
            ],
        ], $data);

    }

    /**
     * @param ApiTester $i
     */
    public function tryToGetMailingSender(ApiTester $i)
    {
        $i->amAuthenticatedByApiKey('has-role-manage-mailing-sender@mail.ru', '12345');
        $i->wantTo('get mailing template via API from /mailing/sender/{id}');
        $i->testApiRequest(
            '/mailing/sender/3',
            [],
            [
                'result' => 'success',
                'data' => [
                    'id' => 3,
                    'email'=> 'Third@mail.ru',
                    'name'=> 'Third',
                ],
            ]
        );
    }
}