<?php
namespace Main\Dashboard\Service\Manager;

use AppBundle\Security\AuthorizationCheckerTrait;
use AppBundle\Security\Role\Permissions;
use EnjoyMailer\Client\Service\EmailsService as ClientEmailsService;
use Main\Dashboard\DTO\MailingSenderData;
use Main\Dashboard\Exception\InvalidSenderEmailException;
use Main\Dashboard\Exception\ObjectNotFoundException;
use Main\Dashboard\Repository\MailingSenderRepository;
use Main\Dashboard\Service\MailingSenderService;
use Main\Validation\ValidatorAwareTrait;

class MailingSenderManager implements MailingSenderService
{
    use AuthorizationCheckerTrait;
    use ValidatorAwareTrait;

    /** @var MailingSenderRepository */
    private $repository;

    /** @var ClientEmailsService */
    private $emailsService;

    /**
     * @param MailingSenderRepository $repository
     * @param ClientEmailsService     $emailsService
     */
    public function __construct(MailingSenderRepository $repository, ClientEmailsService $emailsService)
    {
        $this->repository = $repository;
        $this->emailsService = $emailsService;
    }

    /**
     * {@inheritdoc}
     */
    public function getById(int $id): MailingSenderData
    {
        $this->denyAccessUnlessGranted(Permissions::VIEW_MAILING_SENDER, $id);

        $mailingSender = $this->repository->findById($id);

        if (is_null($mailingSender)) {
            throw new ObjectNotFoundException(ObjectNotFoundException::MAILING_SENDER_OBJECT, ['id' => $id]);
        }

        return $mailingSender;
    }

    /**
     * {@inheritdoc}
     */
    public function list(): array
    {
        $this->denyAccessUnlessGranted(Permissions::LIST_MAILING_SENDER);

        return $this->repository->all();
    }

    /**
     * {@inheritdoc}
     */
    public function create(MailingSenderData $data): MailingSenderData
    {
        $this->denyAccessUnlessGranted(Permissions::CREATE_MAILING_SENDER);
        $this->validate($data);

        return $this->repository->create($data);
    }

    /**
     * {@inheritdoc}
     */
    public function update(int $id, MailingSenderData $data): MailingSenderData
    {
        $this->denyAccessUnlessGranted(Permissions::EDIT_MAILING_SENDER, $id);

        $mailingSenderData = $this->getById($id);

        if (null !== $data->email) {
            $mailingSenderData->email = $data->email;
        }

        if (null !== $data->name) {
            $mailingSenderData->name = $data->name;
        }

        $this->validate($mailingSenderData);
        $this->repository->update($mailingSenderData);

        return $mailingSenderData;
    }

    /**
     * {@inheritdoc}
     */
    public function remove(int $id)
    {
        $this->denyAccessUnlessGranted(Permissions::REMOVE_MAILING_SENDER, $id);

        if ($this->getById($id)) {
            $this->repository->remove($id);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isValidEmail(string $email): bool
    {
        $availableEmails = $this->emailsService->getAvailableEmails();

        return in_array($email, $availableEmails);
    }

    /**
     * {@inheritdoc}
     */
    public function checkValidEmailsBySenders(array $senders): void
    {
        $availableEmails = $this->emailsService->getAvailableEmails();

        $invalidSenders = [];
        foreach ($senders as $sender) {
            if (!in_array($sender->email, $availableEmails)) {
                $invalidSenders[$sender->id] = $sender;
            }
        }

        if (count($invalidSenders)) {
            throw new InvalidSenderEmailException(array_values($invalidSenders));
        }
    }
}
