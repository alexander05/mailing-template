<?php

class Html2TxtMailingTemplateCest
{
    public function _before(ApiTester $i)
    {
        $i->loadFixtures('MailingTemplate');
    }

    /**
     * @param ApiTester $i
     *
     */
    public function tryToConvertHtml2Txt(ApiTester $i)
    {
        $i->amAuthenticatedByApiKey('list-all-mailing-template@mail.ru', '12345');
        $i->wantTo('convert html to txt via API from /mailing/template/html2txt');
        $i->testApiRequest(
            '/mailing/template/html2txt',
            [
                'payload' => [
                    'data'=> '<p>The line</p><p>The <b>other</b> line</p>'
                ],
            ],
            [
                'result' => 'success',
                'data' => "The line\n
The other line",
            ]
        );

    }

}