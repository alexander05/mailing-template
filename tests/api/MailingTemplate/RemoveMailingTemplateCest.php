<?php

class RemoveMailingTemplateCest
{
    public function _before(ApiTester $i)
    {
        $i->loadFixtures('MailingTemplate');
    }

    /**
     * @param ApiTester $i
     */
    public function tryToRemoveNotAuthorized(ApiTester $i)
    {
        $i->amAuthenticatedByApiKey('list-all-mailing-template@mail.ru', '12345');
        $i->wantTo('get access deniend error when removing mailing template via API from /mailing/template/{id}/remove');
        $i->testApiRequest(
            '/mailing/template/1/remove',
            [],
            [
                'result' => 'error',
                'error' => 'ACCESS-DENIED',
                'data' => [
                    'attributes' => ['REMOVE_MAILING_TEMPLATE'],
                    'subject' => 1,
                ],
            ]
        );
    }

    /**
     * @param ApiTester $i
     */
    public function tryToRemoveNonExistentMailingTemplate(ApiTester $i)
    {
        $i->amAuthenticatedByApiKey('has-role-manage-mailing-template@mail.ru', '12345');
        $i->wantTo('get error when removing non-existent mailing template via API from /mailing/template/{id}/remove');
        $i->testApiRequest(
            '/mailing/template/999/remove',
            [],
            [
                'result' => 'error',
                'error' => 'OBJECT-NOT-FOUND',
                'data' => [
                    'type' => 'mailing_template',
                    'identity' => [
                        'id' => 999
                    ],
                ],
            ]
        );
    }

    /**
     * @param ApiTester $i
     */
    public function tryToRemoveSuccess(ApiTester $i)
    {
        $i->amAuthenticatedByApiKey('has-role-manage-mailing-template@mail.ru', '12345');
        $i->wantTo('remove mailing template via API from /mailing/template/{id}/remove');
        $i->testApiRequest(
            '/mailing/template/1/remove',
            [],
            [
                'result' => 'success',
            ]
        );

        $i->dontSeeInDatabase('mailing_template', [
            'mailing_template_id' => 1,
            'title' => 'new test mailing template text',
        ]);
    }
}